db.users.insertMany([
		{
			"firstName": "Mark",
			"lastName": "Feehily",
			"email": "mark@mail.com",
			"password": "mark123",
                        "isAdmin": false
                        
		},
		{
			"firstName": "Brian",
			"lastName": "McFadden",
			"email": "brian@mail.com",
			"password": "brian123",
                        "isAdmin": false
		},
                
                {
                        "firstName": "Shane",
			"lastName": "Fillan",
			"email": "shane@mail.com",
			"password": "shane123",
                        "isAdmin": false
                    
                    
                    
                    
                },
                {
                    
                        "firstName": "Kian",
			"lastName": "Egan",
			"email": "kian@mail.com",
			"password": "kian123",
                        "isAdmin": false
                    
                    
                    
                    },
                    {
                        
                        
                        "firstName": "Nicky",
			"lastName": "Byrne",
			"email": "nicky@mail.com",
			"password": "nicky123",
                        "isAdmin": false
                        
                        
                        
                        
                        }
                                
                
])
                        
                        
 db.courses.insertMany([
		{
			"firstName": "JavaScript 101",
			"price": 5000,
			"isActive": false
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"isActive": false
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"isActive": false
		}
])
                
                
                
                
                db.users.updateOne(
		{
			"firstName": "Mark",
		},
		{
			$set: {
				"isAdmin": true
			}
		}
)
                
                
                db.courses.updateOne(
		{
			"firstName": "JavaScript 101",
		},
		{
			$set: {
				"isActive": true
			}
		}
)
                
                
  db.courses.deleteMany({isActive: false})

                